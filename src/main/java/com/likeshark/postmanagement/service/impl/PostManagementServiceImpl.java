package com.likeshark.postmanagement.service.impl;


import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.likeshark.postmanagement.dto.PostDTO;
import com.likeshark.postmanagement.firebase.FirebaseInitializer;
import com.likeshark.postmanagement.service.PostManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service
public class PostManagementServiceImpl implements PostManagementService {

    @Autowired
    private FirebaseInitializer firebase;

    @Override
    public List<PostDTO> list() {
        return null;
    }

    @Override
    public Boolean add(PostDTO post) {
        Map<String, Object> docData = new HashMap<>();
        docData.put("title", post.getTitle());
        docData.put("content", post.getContent());

        CollectionReference posts = firebase.getFirestore().collection("post");
        ApiFuture<WriteResult> writeResultApiFuture = posts.document().create(docData);

        try {
            if (null != writeResultApiFuture.get())
            {
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }

    }

    @Override
    public Boolean edit(String id, PostDTO post) {
        return null;
    }

    @Override
    public Boolean delete(String id) {
        return null;
    }
}
