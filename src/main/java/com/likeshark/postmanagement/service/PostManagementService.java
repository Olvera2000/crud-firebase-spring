package com.likeshark.postmanagement.service;

import com.likeshark.postmanagement.PostManagementApplication;
import com.likeshark.postmanagement.dto.PostDTO;

import java.util.List;

public interface PostManagementService {
    List<PostDTO> list();
    Boolean add(PostDTO post);
    Boolean edit(String id,PostDTO post);
    Boolean delete(String id);
}
