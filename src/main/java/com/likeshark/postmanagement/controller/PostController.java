package com.likeshark.postmanagement.controller;

import com.likeshark.postmanagement.dto.PostDTO;
import com.likeshark.postmanagement.service.PostManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/post")
public class PostController {
    @Autowired
    private PostManagementService service;


    @GetMapping(value = "/greet/{name}")
    public String greet(@PathVariable(value = "name") String name){
        return "Hello, "+name;
    }

    @GetMapping(value = "/list")
    public ResponseEntity list(){
        return new ResponseEntity(null, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity add(@RequestBody PostDTO post){
        return new ResponseEntity(service.add(post), HttpStatus.OK);
    }
    @PutMapping(value = "/{id}/update")
    public ResponseEntity edit(@PathVariable(value = "id")String id, @RequestBody PostDTO post){

        return new ResponseEntity(null, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}/delete")
    public ResponseEntity delete(@PathVariable(value = "id") String id){

        return new ResponseEntity(null, HttpStatus.OK);
    }
}
