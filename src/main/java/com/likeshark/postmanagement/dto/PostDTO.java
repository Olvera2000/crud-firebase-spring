package com.likeshark.postmanagement.dto;

import lombok.Data;

@Data
public class PostDTO {
    private String id;
    private String title;
    private String content;
}
